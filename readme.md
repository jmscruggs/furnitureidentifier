# FurnitureIdentifier

## Overview:

### FurnitureIdentifier is a mobile friendly web application utilizing a machine learning model that allows a user to upload images of a few furniture items and predicts what furniture item the image is of. The machine learning model was built utilizing transfer learning.

## Technology Utilizied:
### Backend technology
* C# - .NET Core 3.1 (ML.Net for building the machine learning model)
### Frontend technology
* Javascript, HTML, CSS, and razor syntax
### Dev-ops technology
* GitLab (building/testing application)
* AWS (Elastic Beanstalk to host application)

## Current Contributors:
* James Scruggs
* Sam Hollingsworth
* Cody Maness
* Girgis Shihataa
* Alex Bellina
* Emily Bridgers
* Joey Cooper
* Robert Smith