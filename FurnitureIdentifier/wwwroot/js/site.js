﻿
//dictionary to match output from the Predict API
var dict_output = {
    "bed": "Bed",
    "chair": "Chair",
    "sofa": "Sofa",
    "swivelchair": "Swivel Chair",
    "table": "Table"
};

$(document).ready(function () {

    $("#imageBrowes").change(function () {

        var File = this.files;

        if (File && File[0]) {
            ReadImage(File[0]);
        }

    });
    $("#open_about").on("click", function ()
    {
        $('#about').dialog("open");
        $('.ui-dialog :button').blur();
    });

    $('#about').dialog({
        draggable: false,
        resize: false,
        autoOpen: false,
        title: "About Us"
    });
});

// this is the id of the form
$("#imageForm").submit(function (e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.
    $("#loader").show()
    var form = $(this);
    var url = form.attr('action');

    var fdata = new FormData();

    var file = $(this).find('input:file[id="imageBrowes"]')[0].files[0];

    fdata.append("image", file);

    $.ajax({
        type: "POST",
        url: url,
        data: fdata, // serializes the form's elements.
        processData: false,
        contentType: false,
        success: function (data) {
            $(".upload-header").html("Results")
            $(".submit-btn").hide();
            $("#loader").hide();
            $("#prediction").show();
            $(".upload-btn-wrapper .upload-btn").html("Upload Again");
            $("#prediction").html("We predict your image is a <b>" + data.label + "</b> with <b>" + (data.confidence * 100).toFixed(2) + "% confidence</b>.");
            $("#imageBrowes").val(null);
        },
        error: function (data) {
            $(".upload-header").html("Results")
            $(".submit-btn").hide();
            $("#loader").hide();
            $("#prediction").show();
            $(".upload-btn-wrapper .upload-btn").html("Upload Again");
            $("#prediction").html("An error has encountered");
            $("#imageBrowes").val(null);
        }
    });
});

var ReadImage = function (file) {

    var reader = new FileReader;
    var image = new Image;
    $("#prediction").hide();
    $(".upload-btn-wrapper .upload-btn").html("Change");
    $(".submit-btn").show();
    $(".upload-header").html("Upload a Furniture Image");

    reader.readAsDataURL(file);
    reader.onload = function (_file) {

        image.src = _file.target.result;
        image.onload = function () {

            var height = this.height;
            var width = this.width;
            var type = file.type;
            var size = ~~(file.size / 1024) + "KB";

            $("#targetImg").attr('src', _file.target.result);
            $("#imgPreview").show();

        }

    }

}

var ClearPreview = function () {
    $("#imageBrowes").val('');
    $("#description").text('');
    $("#imgPreview").hide();
    $(".upload-btn-wrapper").show();
}


