﻿using System.IO;

namespace FurnitureIdentifier.Extensions
{
    public static class StreamExtension
    {
        public static byte[] ToByteArray(this Stream myStream)
        {
            using (var memoryStream = new MemoryStream())
            {
                myStream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}
