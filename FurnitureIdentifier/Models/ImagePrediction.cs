﻿using Microsoft.ML.Data;

namespace FurnitureIdentifier.Models
{
    public class ImagePrediction : ImageData
    {
        [ColumnName("PredictedLabel")]
        public string Prediction { get; set; }

        [VectorType(5)]
        public float[] Score { get; set; }
    }
}
