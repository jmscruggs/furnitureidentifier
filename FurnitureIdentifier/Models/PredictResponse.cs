﻿namespace FurnitureIdentifier.Models
{
    public class PredictResponse
    {
        public string Label { get; set; }
        public float Confidence { get; set; }
    }
}
