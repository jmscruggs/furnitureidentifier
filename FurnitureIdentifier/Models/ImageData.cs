﻿using Microsoft.ML.Data;

namespace FurnitureIdentifier.Models
{
    public class ImageData
    {
        [LoadColumn(0)]
        public byte[] Image;
    }
}
