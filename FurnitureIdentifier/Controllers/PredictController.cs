﻿using Microsoft.AspNetCore.Mvc;
using FurnitureIdentifier.Models;
using Microsoft.Extensions.ML;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.Extensions.Logging;
using FurnitureIdentifier.Extensions;
using System.Linq;

namespace FurnitureIdentifier.Controllers
{
    [Route("api/[controller]")]
    [Consumes("application/octet-stream", "multipart/form-data")]
    [ApiController]
    public class PredictController : ControllerBase
    {
        private readonly PredictionEnginePool<ImageData, ImagePrediction> _predictionEnginePool;

        private readonly ILogger<PredictController> _logger;

        public PredictController(PredictionEnginePool<ImageData, ImagePrediction> predictionEnginePool, ILogger<PredictController> logger)
        {
            _predictionEnginePool = predictionEnginePool;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult<PredictResponse> Post([FromForm] IFormFile image)
        {
            ImagePrediction prediction = new ImagePrediction();
            try
            {
                var input = new ImageData
                {
                    Image = image.OpenReadStream().ToByteArray()
                };
                prediction = _predictionEnginePool.Predict(modelName: "FurnitureModel", example: input);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            var response = new PredictResponse
            {
                Label = prediction.Prediction, 
                Confidence = prediction.Score.ToList().Max()
            };

            return Ok(response);
        }
    }
}
